import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
import pandas as pd
from pandas import tseries
from datetime import datetime
from datetime import timedelta
import seaborn as sns
import thinkbayes2 as tb

def unique(a):
    """ return the list with duplicate elements removed """
    return list(set(a))

def intersect(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))

def union(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))

def timeConvertGMT(inputDateString):
    datetime_object = datetime.strptime(inputDateString, '%Y-%m-%dT%H:%M:%SZ')
    return datetime_object

def timeConvertLocal(inputDateString):
    datetime_object = datetime.strptime(inputDateString, '%B %d, %Y %I:%M:%S%p')
    return datetime_object

def GetReporterData():
    reporter = pd.read_csv('reporter-export.csv')
    reporter = reporter.rename(columns = {'Timestamp of Report (GMT)' : 'GMT',
      'Timestamp of Report (Local Time)' : 'LocalTime',
      'Number of Photos Added' : 'PhotoCount',
      'Ambient Audio (dB)' : 'AudioDB',
      'Ambient Audio Description' : 'AudioBIN',
      'Number of Steps' : 'Steps',
      'How did you sleep?' : 'Slept',
      'How Do You Feel Waking Up?' : 'Wake.feel',
      'Who are you with?' : 'Companions',
      'How many teas did you have today?' : 'TeaCount',
      'What are you doing?' : 'Activity',
      'Are you working?' : 'Work',
      'Is it Going Well?' : 'Work.well',
      'Where are you?' : 'Location',
      'Are You Satisfied With How Much You\'re Getting Done Today?' : 'Satisfaction'})
    reporter = reporter.drop(['Note something good that happened today:', 'What did you learn today?'], axis = 1)
    reporter['GMT'] = reporter['GMT'].apply(timeConvertGMT)
    reporter['LocalTime'] = reporter['LocalTime'].apply(timeConvertLocal)
    reporter['SleepDuration'] = calcSleepDuration(reporter)
    reporter = boolWorking(reporter)
    
    # REMOVE OBS MADE IN ERROR
    error_locs = [300, 300-1]
    reporter.drop(error_locs, inplace=True)
    
    # Remove data recorded today
    reporter[reporter['LocalTime'].dt.date != datetime.today().date()]
    
    dayReporter = extractDay(reporter)
    sleepReporter = extractSleep(reporter)
    wakeReporter = extractWake(reporter)
    return reporter, dayReporter, sleepReporter, wakeReporter

def extractWake(reporter):
    # Filter data frame for observations containing sleep quality. Store in new data frame.
    return reporter[~reporter['Slept'].isnull()]

def extractSleep(reporter):
    # Filter data frame for observations containing tea count. Store in new data frame.
    return reporter[~reporter['TeaCount'].isnull()]

def extractDay(reporter):
    # Filter data frame for observations not containing either tea or sleep. Store in new data frame.
    return reporter[reporter['TeaCount'].isnull() & reporter['Slept'].isnull()]

def calcSleepDuration(reporter):
    wakeSeries = extractWake(reporter)
    sleepSeries = extractSleep(reporter)
    print sleepSeries.index
    print wakeSeries.index
    #print sleepSeries
    
    while True:
        for i in range(sleepSeries.index.size):
                if (wakeSeries.index[i] - 1) != sleepSeries.index[i]:
                    print sleepSeries.index[i]
                    sleepSeries.drop(sleepSeries.index[i], inplace=True)
                    break
        else:
            break
                    
    while True:
        for i in range(wakeSeries.index.size):
                if (sleepSeries.index[i] + 1) != wakeSeries.index[i]:
                    wakeSeries.drop(wakeSeries.index[i], inplace=True)
                    break
        else:
            break
                    
    print sleepSeries.index
    print wakeSeries.index
    
    sleepSeries.index = wakeSeries.index
    return wakeSeries['GMT'] - sleepSeries['GMT']

def displaySleepDuration(reporter, split = True):
    
    # Prepare Data
    sleepData = analyseSleep(reporter).reset_index()
    sleepLabels = getUnique(sleepData['Slept'])
    print sleepLabels
    numelLabels = len(np.unique(sleepData['Slept']))
    
    # Plot Data, either split by sleepLabels (poorly, etc.) or not.
    if split == True:
        for sLabel in sleepLabels:
            sleepDuration = sleepData['SleepDuration'][sleepData['Slept'].str.contains(sLabel)]
            if not len(sleepDuration) > 1:
                continue
            sleepSeconds = sleepDuration.dt.total_seconds()
            sleepHours = sleepSeconds/60/60
            binwidth = 0.5; binRange = np.arange(min(sleepHours), max(sleepHours) + binwidth, binwidth)
            #plt.hist(sleepHours, bins=binRange, label=sLabel, alpha=1/float(numelLabels))
            sns.distplot(sleepHours, bins=binRange, label=sLabel, kde=False, rug=True);
    else:
        sleepDuration = sleepData['SleepDuration']
        sleepSeconds = sleepDuration.dt.total_seconds()
        sleepHours = sleepSeconds/60/60
        binwidth = 0.5; binRange = np.arange(min(sleepHours), max(sleepHours) + binwidth, binwidth)
        sns.distplot(sleepHours, bins=binRange, kde=False, rug=True);
        #plt.hist(sleepHours, bins=binRange, label='Sleep in Hours', alpha=1)
    plt.legend(loc='upper right')
    
def analyseSleep(reporter):
    wakeSeries = extractWake(reporter)
    moodBeforeSleep = reporter['Mood'][wakeSeries.index-2]; moodBeforeSleep.index = wakeSeries.index
    moodBeforeSleep = moodBeforeSleep.rename('MoodBeforeSleep')
    wakeFrame = pd.concat([wakeSeries['SleepDuration'], wakeSeries['Slept'], wakeSeries['Wake.feel'], moodBeforeSleep], axis=1)
    wakeFrame = wakeFrame[-wakeFrame['Wake.feel'].str.contains('Forgot to Turn Sleep Off', na = False)]
    return wakeFrame.sort_values('Slept')

def boolWorking(reporter):
    for i in range(len(reporter['Work'])):
        if reporter.loc[i, 'Work'] == 'Yes':
            reporter.loc[i, 'Work.bool'] = True
        elif reporter.loc[i, 'Work'] == 'No':
            reporter.loc[i, 'Work.bool'] = False
        else:
            reporter.loc[i, 'Work.bool'] = float('NaN')
    return reporter

def getUnique(txtSeries):
    txtSeries = txtSeries[~txtSeries.isnull()]
    txtSeries = txtSeries.apply(str.split, args=',')
    
    tmpList = list()
    #print tmp
    for i in txtSeries:
        tmpList = union(tmpList, i)
    return np.unique(tmpList)

def getIdxTimeDifference(dateIndexedFrame):
        dateIndexedFrame['indexcol'] = dateIndexedFrame.index
        dateIndexedFrame['Delta'] = dateIndexedFrame['indexcol'].diff()
        return dateIndexedFrame['Delta']

def populateBinaryCols(dataSeries):
    df = pd.DataFrame()
    unique = getUnique(dataSeries)
    for i in unique:
        df[i] = dataSeries.str.contains(i)         
    return df
    
def displayToken(reporter, tokenType, token, display='Time', ax=None):
    
    if tokenType != 'Mood' and tokenType != 'Activity':
        raise ValueError('Invalid Token Type')
    
    oneHotToken = populateBinaryCols(reporter[tokenType])
    oneHotToken.index = reporter['LocalTime']
    tokenSeries = oneHotToken[token]
    tokenSeries = tokenSeries.astype('float').fillna(method='ffill')
    print 
    
    # What does y axis for date mean?
    if display == 'Date':
        resampledSeries = tokenSeries.resample('D')
        resampledSeries.mean().plot(ax=ax, title=token);
        return
    
    elif display=='Day':
        dayIndex = range(0,7)
        strDayIndex = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        meanToken = dict()
        
        for day in dayIndex:
            meanToken[day] = tokenSeries[tokenSeries.index.dayofweek == day].mean()
        
        plt.plot(meanToken.keys(), meanToken.values()); plt.xticks(meanToken.keys(), strDayIndex);
    
    elif display=='Time':
        dateIndex = list()
        meanToken = list()

        for i in range(0, 24, 1):
            #print i
            start = str(i)+':00'
            end = str(i)+':59'
            #print i
            dateIndex.append(pd.to_datetime(start))
            meanToken.append(tokenSeries.between_time(start, end).mean())

        tokenHrs = pd.DataFrame(data = meanToken, index = dateIndex)
        #print tokenHrs
        #plt.figure()
        #plt.scatter(tokenHrs.index.time+pd.datetools.Minute(30), tokenHrs);
        resampledSeries = tokenHrs.resample('H')
        
        # push plot forwards by five hours
        resampledSeries = resampledSeries.mean()
        idx = resampledSeries.index
        resampledSeries = resampledSeries.iloc[np.mod(range(5,29,1),24)]
        resampledSeries.index = idx
        resampledSeries = resampledSeries.shift(freq=timedelta(hours=5))
        
        # plot
        resampledSeries.plot(ax=ax, title=token); 
        plt.ylim(0,1);
        plt.ylabel('Mean Percentage of time spent '+token)
        plt.xlabel('Time of Day')
        #plt.scatter(tmp.time, tokenHrs);
        return
    
    else:
        raise  ValueError('Invalid Display Type')
        return
    
def extractTokenObs(reporter, tokenType, token):
    # Make a one-hot matrix
    oneHotToken = populateBinaryCols(reporter[tokenType])
    
    # Get the index value of all of the appropriate obs
    idx = oneHotToken[oneHotToken[token] == True].index
    return reporter.loc[idx]
    
def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    #print s
    is_max = s == s.max(axis=0)
    return ['background-color: yellow' if v else '' for v in is_max]

def heatmap(df):

    cm = sns.light_palette("green", as_cmap=True)

    s = df.style.background_gradient(cmap=cm)
    return s

def tokenDailyResampled(reporter, tokenType, token, sampleType='mean'):
    oneHotToken = populateBinaryCols(reporter[tokenType])
    oneHotToken.index = reporter['LocalTime']
    tokenSeries = oneHotToken[token]
    tokenSeries = tokenSeries.astype('float').fillna(method='ffill')
    resampledSeries = tokenSeries.resample('24H', base = 5)
    #print(resampledSeries.sum())
    if sampleType == 'mean':
        return pd.DataFrame(resampledSeries.mean())
    else:
        return pd.DataFrame(resampledSeries.sum())

def targetBayes(reporter, tokenType, token, targetType, target, display=False, alpha=1, beta=1):
    """
    Given a token (e.g. programming) and a target (e.g. content), predicts
    probability of the target given the token.
    Prior is uniform.
    """
    bayes = tb.Beta(alpha, beta)
    obs = extractTokenObs(reporter, tokenType=tokenType, token=token)
    tokenTargetType = populateBinaryCols(obs[targetType])
    tokenTarget = tokenTargetType[target].sum()
    tokenNotTarget = obs.shape[0]-tokenTarget
    bayes.Update((tokenTarget, tokenNotTarget))
    
    if display==True:
        samples = np.zeros(len(range(0,1000)))
        for i in (range(0,1000)):
            samples[i] = bayes.EvalPdf(i/1000.0)
        plt.plot(samples);
        plt.xticks([0, 200, 400, 600, 800, 1000], [0, 0.2, 0.4, 0.6, 0.8, 1])
        plt.tick_params(
        axis='y',
        which='both',
        labelleft='off')
        plt.ylabel('Probability Density')
        plt.xlabel('Probability of (' + str(target) + ') Given ' + str(token))
    
    return bayes