
# coding: utf-8

# # Setup

# In[16]:

get_ipython().magic(u'matplotlib inline')

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
import pandas as pd
from pandas import tseries
from datetime import datetime
from datetime import timedelta
import seaborn as sns

import reporterFun as rf

reporter = rf.GetReporterData()


# # Analyse Mood Patterns

# In[7]:

rf.displayToken(reporter, 'Mood', 'Bored', display='Day')


# Moods will need to be mashed together.
# For example:
# 
# ##### Positive vs. Negative
# 
# Positive: Calm, Content, Excited, Focused, Happy, Impressed, Interested, Productive, Quiet, Satisfied
# 
# Negative: Ashamed, Bored, Distracted, Embarrassed, Irritated, Nervous, Reluctant, Restless, Sad, Stressed, Unproductive, Unsatisfied, Upset

# In[17]:

uniqueMoods = rf.getUnique(reporter['Mood'])
print uniqueMoods

moodHot = rf.populateBinaryCols(reporter['Mood'])


# Add values or simply create one hot for any positive mood?

# In[18]:

positiveMood = moodHot['Calm'] + (moodHot['Content'] + moodHot['Excited'] + moodHot['Focused'] + moodHot['Happy'] + 
                                  moodHot['Impressed'] + moodHot['Interested'] + moodHot['Productive'] +
                                  moodHot['Satisfied'])

positiveMood2 = moodHot['Calm'] | (moodHot['Content'] | moodHot['Excited'] | moodHot['Focused'] | moodHot['Happy'] | 
                                  moodHot['Impressed'] | moodHot['Interested'] | moodHot['Productive'] |
                                  moodHot['Satisfied'])


# Pairwise correlation between moods doesn't work because they're binary, so there are only four possible combinations.

# In[19]:

moodHot = rf.populateBinaryCols(reporter['Mood'])

#moodHot.iloc[:,0:3]
#plot_moods = list(moodHot.loc[:,'Ashamed':'Bored']) + ['Ill']
plot_moods = ['Tired'] + ['Distracted']
#sns.pairplot(moodHot[plot_moods])


# Resample moods to aggregate them over a single day (beginning at 5 o'clock). This should give a distribution that can be correlated: being tired more over a single day can be correlated with being bored more over a single day. Problem that few tokens have high numbers within a day, making the regression unreliable.

# In[32]:

uniqueMoods = rf.getUnique(reporter['Mood'])
print uniqueMoods
moodHot = rf.populateBinaryCols(reporter['Mood'])
moodHot.index = reporter['LocalTime']
resampledMood = moodHot.resample('24H', base = 5)
resampledMood = resampledMood.sum()

#sns.pairplot(resampledMood.loc[:, (resampledMood.sum() > 40)], kind="reg") # pairwise comparison of moods which have more than 10 datapoints


# Now try with some merged datasets:

# In[39]:

uniqueMoods = rf.getUnique(reporter['Mood'])
print uniqueMoods
moodHot = rf.populateBinaryCols(reporter['Mood'])
moodHot.index = reporter['LocalTime']
resampledMood = moodHot.resample('24H', base = 5)

resampledMood = resampledMood.sum()
resampledMood['Restless']+=resampledMood['Distracted']; del resampledMood['Distracted']
resampledMood['Guilty']+=resampledMood['Ashamed']+resampledMood['Embarrassed']; del resampledMood['Ashamed']; del resampledMood['Embarrassed']
resampledMood['Frustrated']+=resampledMood['Impatient']; del resampledMood['Impatient']
resampledMood['Nervous']+=resampledMood['Worried']; del resampledMood['Worried']

sns.pairplot(resampledMood.loc[:, (resampledMood.sum() > 40)], kind="reg") # pairwise comparison of moods which have more than 10 datapoints


# In[ ]:



