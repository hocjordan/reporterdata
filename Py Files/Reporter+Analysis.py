
# coding: utf-8

# # Headers and Definitions

# In[1]:

get_ipython().magic(u'matplotlib inline')

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
import pandas as pd
from pandas import tseries
from datetime import datetime
from datetime import timedelta
import seaborn as sns


# In[2]:

def unique(a):
    """ return the list with duplicate elements removed """
    return list(set(a))

def intersect(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))

def union(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))

def timeConvertGMT(inputDateString):
    datetime_object = datetime.strptime(inputDateString, '%Y-%m-%dT%H:%M:%SZ')
    return datetime_object

def timeConvertLocal(inputDateString):
    datetime_object = datetime.strptime(inputDateString, '%B %d, %Y %I:%M:%S%p')
    return datetime_object

def GetReporterData():
    reporter = pd.read_csv('reporter-export.csv')
    reporter = reporter.rename(columns = {'Timestamp of Report (GMT)' : 'GMT',
      'Timestamp of Report (Local Time)' : 'LocalTime',
      'Number of Photos Added' : 'PhotoCount',
      'Ambient Audio (dB)' : 'AudioDB',
      'Ambient Audio Description' : 'AudioBIN',
      'Number of Steps' : 'Steps',
      'How did you sleep?' : 'Slept',
      'Who are you with?' : 'Companions',
      'How many teas did you have today?' : 'TeaCount',
      'What are you doing?' : 'Activity',
      'Are you working?' : 'Work',
      'Where are you?' : 'Location',
      'Are You Satisfied With How Much You\'re Getting Done Today?' : 'Satisfaction'})
    reporter = reporter.drop(['Note something good that happened today:', 'What did you learn today?'], axis = 1)
    reporter['GMT'] = reporter['GMT'].apply(timeConvertGMT)
    reporter['LocalTime'] = reporter['LocalTime'].apply(timeConvertLocal)
    reporter['SleepDuration'] = calcSleepDuration(reporter)
    reporter = boolWorking(reporter)
    return reporter

def extractWake(reporter):
    # Filter data frame for observations containing sleep quality. Store in new data frame.
    return reporter[~reporter['Slept'].isnull()]

def extractSleep(reporter):
    # Filter data frame for observations containing tea count. Store in new data frame.
    return reporter[~reporter['TeaCount'].isnull()]

def extractDay(reporter):
    # Filter data frame for observations not containing either tea or sleep. Store in new data frame.
    return reporter[reporter['TeaCount'].isnull() & reporter['Slept'].isnull()]

def calcSleepDuration(reporter):
    wakeSeries = extractWake(reporter)
    sleepSeries = extractSleep(reporter)
    sleepSeries.index = wakeSeries.index
    return wakeSeries['GMT'] - sleepSeries['GMT']

def displaySleepDuration(reporter, split = True):
    
    # Prepare Data
    sleepData = analyseSleep(reporter).reset_index()
    sleepLabels = np.unique(sleepData['Slept'])
    numelLabels = len(np.unique(sleepData['Slept']))
    
    # Plot Data
    if split == True:
        for sLabel in sleepLabels:
            sleepDuration = sleepData['SleepDuration'][sleepData['Slept']==sLabel]
            if not len(sleepDuration) > 1:
                continue
            sleepSeconds = sleepDuration.dt.total_seconds()
            sleepHours = sleepSeconds/60/60
            binwidth = 0.5; binRange = np.arange(min(sleepHours), max(sleepHours) + binwidth, binwidth)
            #plt.hist(sleepHours, bins=binRange, label=sLabel, alpha=1/float(numelLabels))
            sns.distplot(sleepHours, bins=binRange, label=sLabel, kde=False, rug=True);
    else:
        sleepDuration = sleepData['SleepDuration']
        sleepSeconds = sleepDuration.dt.total_seconds()
        sleepHours = sleepSeconds/60/60
        binwidth = 0.5; binRange = np.arange(min(sleepHours), max(sleepHours) + binwidth, binwidth)
        sns.distplot(sleepHours, bins=binRange, kde=False, rug=True);
        #plt.hist(sleepHours, bins=binRange, label='Sleep in Hours', alpha=1)
    plt.legend(loc='upper right')
    
def analyseSleep(reporter):
    wakeSeries = extractWake(reporter)
    moodBeforeSleep = reporter['Mood'][wakeSeries.index-2]; moodBeforeSleep.index = wakeSeries.index
    moodBeforeSleep = moodBeforeSleep.rename('MoodBeforeSleep')
    wakeFrame = pd.concat([wakeSeries['SleepDuration'], wakeSeries['Slept'], moodBeforeSleep], axis=1)
    return wakeFrame.sort_values('Slept')

def boolWorking(reporter):
    for i in range(len(reporter['Work'])):
        if reporter.loc[i, 'Work'] == 'Yes':
            reporter.loc[i, 'Work'] = True
        elif reporter.loc[i, 'Work'] == 'No':
            reporter.loc[i, 'Work'] = False
        else:
            reporter.loc[i, 'Work'] = float('NaN')
    return reporter

def getUnique(txtSeries):
    txtSeries = txtSeries[~txtSeries.isnull()]
    txtSeries = txtSeries.apply(str.split, args=',')
    
    tmpList = list()
    #print tmp
    for i in txtSeries:
        tmpList = union(tmpList, i)
    return np.unique(tmpList)

def populateBinaryCols(dataSeries):
    df = pd.DataFrame()
    unique = getUnique(dataSeries)
    for i in unique:
        df[i] = dataSeries.str.contains(i)         
    return df
    
def displayToken(reporter, tokenType, token, display='Time', ax=None):
    
    if tokenType != 'Mood' and tokenType != 'Activity':
        raise ValueError('Invalid Token Type')
    
    oneHotToken = populateBinaryCols(reporter[tokenType])
    oneHotToken.index = reporter['LocalTime']
    tokenSeries = oneHotToken[token]
    tokenSeries = tokenSeries.astype('float').fillna(method='ffill')
    
    # What does y axis for date mean?
    if display == 'Date':
        resampledSeries = tokenSeries.resample('D')
        resampledSeries.mean().plot(ax=ax, title=token);
        return
    
    elif display=='Time':

        dateIndex = list()
        meanToken = list()

        for i in range(0, 24, 1):
            #print i
            start = str(i)+':00'
            end = str(i)+':59'
            #print i
            dateIndex.append(pd.to_datetime(start))
            meanToken.append(tokenSeries.between_time(start, end).mean())

        tokenHrs = pd.DataFrame(data = meanToken, index = dateIndex)
        #print tokenHrs
        #plt.figure()
        #plt.scatter(tokenHrs.index.time+pd.datetools.Minute(30), tokenHrs);
        resampledSeries = tokenHrs.resample('H')
        
        # push plot forwards by five hours
        resampledSeries = resampledSeries.mean()
        idx = resampledSeries.index
        resampledSeries = resampledSeries.iloc[np.mod(range(5,29,1),24)]
        resampledSeries.index = idx
        resampledSeries = resampledSeries.shift(freq=timedelta(hours=5))
        
        # plot
        resampledSeries.plot(ax=ax, title=token); 
        plt.ylim(0,1);
        plt.ylabel('Mean Percentage of time spent '+token)
        plt.xlabel('Time of Day')
        #plt.scatter(tmp.time, tokenHrs);
        return
    
    else:
        raise  ValueError('Invalid Display Type')
        return
    
def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    #print s
    is_max = s == s.max(axis=0)
    return ['background-color: yellow' if v else '' for v in is_max]

def heatmap(df):

    cm = sns.light_palette("green", as_cmap=True)

    s = df.style.background_gradient(cmap=cm)
    return s


# # Get Data
# 
# Read in data from reporter_export.csv and store in Pandas DataFrame

# In[3]:

reporter = GetReporterData()


# In[4]:

reporter


# # Analyse Sleeping Patterns

# What does basic sleep data look like?

# In[5]:

#print analyseSleep(reporter)
displaySleepDuration(reporter)


# How do my energy levels vary over time?

# In[6]:

displayToken(reporter, 'Mood', 'Tired', display='Time')


# In[7]:

displayToken(reporter, 'Mood', 'Tired', display='Date');
#fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=False)
#displayToken(reporter, 'Mood', 'Tired', display='Time', ax=ax1)
#displayToken(reporter, 'Mood', 'Tired', display='Date', ax=ax2)


# # Analyse Mood Patterns

# In[8]:

displayToken(reporter, 'Mood', 'Bored')


# Moods will need to be mashed together.
# For example:
# 
# ##### Positive vs. Negative
# 
# Positive: Calm, Content, Excited, Focused, Happy, Impressed, Interested, Productive, Quiet, Satisfied
# 
# Negative: Ashamed, Bored, Distracted, Embarrassed, Irritated, Nervous, Reluctant, Restless, Sad, Stressed, Unproductive, Unsatisfied, Upset

# In[9]:

uniqueMoods = getUnique(reporter['Mood'])
print uniqueMoods

moodHot = populateBinaryCols(reporter['Mood'])


# Add values or simply create one hot for any positive mood?

# In[83]:

positiveMood = moodHot['Calm'] + (moodHot['Content'] + moodHot['Excited'] + moodHot['Focused'] + moodHot['Happy'] + 
                                  moodHot['Impressed'] + moodHot['Interested'] + moodHot['Productive'] +
                                  moodHot['Satisfied'])

positiveMood2 = moodHot['Calm'] | (moodHot['Content'] | moodHot['Excited'] | moodHot['Focused'] | moodHot['Happy'] | 
                                  moodHot['Impressed'] | moodHot['Interested'] | moodHot['Productive'] |
                                  moodHot['Satisfied'])

print positiveMood2


# Pairwise correlation between moods doesn't work because they're binary, so there are only four possible combinations.

# In[122]:

moodHot = populateBinaryCols(reporter['Mood'])

#moodHot.iloc[:,0:3]
#plot_moods = list(moodHot.loc[:,'Ashamed':'Bored']) + ['Ill']
plot_moods = ['Tired'] + ['Distracted']
#sns.pairplot(moodHot[plot_moods])


# Resample moods to aggregate them over a single day (beginning at 5 o'clock). This should give a distribution that can be correlated: being tired more over a single day can be correlated with being bored more over a single day.

# In[ ]:

uniqueMoods = getUnique(reporter['Mood'])
print uniqueMoods
moodHot = populateBinaryCols(reporter['Mood'])
moodHot.index = reporter['LocalTime']
resampledMood = moodHot.resample('24H', base = 5)

tiredIdx = 24
boredIdx = 1
#print resampledMood.sum().iloc[:,boredIdx]
resampledMood = resampledMood.sum()
#print resampledMood.loc[:, (resampledMood.sum() > 10)].sum()
#print resampledMood.loc[:, (resampledMood.sum() > 10)]
#sns.pairplot(resampledMood.loc[:, (resampledMood.sum() > 10)], kind="reg") # pairwise comparison of moods which have more than 10 datapoints


# Now try with some merged datasets:

# In[10]:

uniqueMoods = getUnique(reporter['Mood'])
print uniqueMoods
moodHot = populateBinaryCols(reporter['Mood'])
moodHot.index = reporter['LocalTime']
resampledMood = moodHot.resample('24H', base = 5)

resampledMood = resampledMood.sum()
resampledMood['Restless']=resampledMood['Restless']+resampledMood['Distracted']; del resampledMood['Distracted']
resampledMood['Interested']=resampledMood['Excited']+resampledMood['Interested']; del resampledMood['Excited']

sns.pairplot(resampledMood.loc[:, (resampledMood.sum() > 10)], kind="reg") # pairwise comparison of moods which have more than 10 datapoints


# # Analyse Activity Patterns

# In[45]:

displayToken(reporter, 'Activity', 'Programming', display='Date')


# In[46]:

displayToken(reporter, 'Activity', 'Academic Writing', display='Date')


# # Analyse Working Patterns

# In[12]:

uniqueMoods = getUnique(reporter['Mood'])
uniqueActs = getUnique(reporter['Activity'])
#print uniqueMoods
#print uniqueActs
        
oneHotMood = populateBinaryCols(reporter['Mood'])
onehotActivity = populateBinaryCols(reporter['Activity'])


# When do I work?

# In[ ]:




# Does being tired negatively affect work time, satisfaction, focus or productivity?
# 
# What's the question? Whether or not being tired affects how much I work. How to measure? Naive would be to check if I'm doing more work when I'm tired or not, but that would be silly; obviously doing work makes me tired and so they're correlated. Perhaps would be better to do whether or not being tired makes the next observation a work observation.

# In[13]:

# Get all cases of being tired
moodHot = populateBinaryCols(reporter['Mood'])
tiredObs = moodHot[moodHot['Tired'] == True].index
print tiredObs


# What moods are correlated with working?

# In[14]:

workObs = reporter[reporter['Work']==True]
nonWorkObs = reporter[reporter['Work']==False]

workMood = populateBinaryCols(workObs['Mood'])
nonWorkMood = populateBinaryCols(nonWorkObs['Mood'])

workMoodMean = workMood.mean();
nonWorkMoodMean = nonWorkMood.mean()
nonWorkMoodMean.name = 'NonWork'

workMoodFrame = pd.DataFrame()
workMoodFrame['Work'] = workMoodMean;
workMoodFrame = workMoodFrame.join(nonWorkMoodMean, how='outer')

#workMoodFrame.style.apply(highlight_max, axis=1)
heatmap(workMoodFrame)


# In[15]:

a =(range(0,24,1))

np.mod(a,20)


# In[ ]:



