
# coding: utf-8

# # Setup

# In[1]:

get_ipython().magic(u'matplotlib inline')

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
import pandas as pd
from pandas import tseries
from datetime import datetime
from datetime import timedelta
import seaborn as sns

import reporterFun as rf

reporter = rf.GetReporterData()


# # Analyse Working Patterns

# In[2]:

uniqueMoods = rf.getUnique(reporter['Mood'])
uniqueActs = rf.getUnique(reporter['Activity'])
#print uniqueMoods
#print uniqueActs
        
oneHotMood = rf.populateBinaryCols(reporter['Mood'])
onehotActivity = rf.populateBinaryCols(reporter['Activity'])


# When do I work?

# In[ ]:




# Does being tired negatively affect work time, satisfaction, focus or productivity?
# 
# What's the question? Whether or not being tired affects how much I work. How to measure? Naive would be to check if I'm doing more work when I'm tired or not, but that would be silly; obviously doing work makes me tired and so they're correlated. Perhaps would be better to do whether or not being tired makes the next observation a work observation.

# In[3]:

# Get all cases of being tired
moodHot = rf.populateBinaryCols(reporter['Mood'])
tiredObs = moodHot[moodHot['Tired'] == True].index
print tiredObs


# What moods are correlated with working?

# In[4]:

workObs = reporter[reporter['Work']==True]
nonWorkObs = reporter[reporter['Work']==False]

workMood = rf.populateBinaryCols(workObs['Mood'])
nonWorkMood = rf.populateBinaryCols(nonWorkObs['Mood'])

workMoodMean = workMood.mean();
nonWorkMoodMean = nonWorkMood.mean()
nonWorkMoodMean.name = 'NonWork'

workMoodFrame = pd.DataFrame()
workMoodFrame['Work'] = workMoodMean;
workMoodFrame = workMoodFrame.join(nonWorkMoodMean, how='outer')

#workMoodFrame.style.apply(highlight_max, axis=1)
rf.heatmap(workMoodFrame)


# 

# In[ ]:



