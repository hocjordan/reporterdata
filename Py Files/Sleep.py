
# coding: utf-8

# # Setup

# In[1]:

get_ipython().magic(u'matplotlib inline')

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import sklearn
import pandas as pd
from pandas import tseries
from datetime import datetime
from datetime import timedelta
import seaborn as sns

import reporterFun as rf

reporter, dayReporter, sleepReporter, wakeReporter = rf.GetReporterData()


# # Analyse Sleeping Patterns

# In[2]:

rf.displaySleepDuration(reporter)


# How do my energy levels vary over time?

# In[3]:

rf.displayToken(reporter, 'Mood', 'Tired', display='Time')


# In[4]:

rf.displayToken(reporter, 'Mood', 'Tired', display='Day')


# In[5]:

rf.displayToken(reporter, 'Mood', 'Tired', display='Date');
#fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=False)
#rf.displayToken(reporter, 'Mood', 'Tired', display='Time', ax=ax1)
#rf.displayToken(reporter, 'Mood', 'Tired', display='Date', ax=ax2)


# Amount tired per day vs. sleep quality.

# In[6]:

# Get Amount Tired
dataf = rf.tokenDailyResampled(dayReporter, 'Mood', 'Tired', sampleType='mean')

# Get Sleep Duration in Hours
wake = wakeReporter.copy()
wake.index = wake['LocalTime']
wake.index = wake.index.date + pd.DateOffset(hours = 5)
wake.loc[:, 'SleepDuration'] = wake['SleepDuration'].dt.seconds/60.0/60.0

# Merge, and Remove Days Which Are Not Consecutive 
wake = wake.merge(dataf, how='inner', right_index=True, left_index=True)
wake = wake[rf.getIdxTimeDifference(wake) == timedelta(days=1)]
sns.swarmplot(x=wake['Slept'], y=wake['Tired'])

sns.plt.figure()
sns.plt.scatter(x=wake['SleepDuration'], y=wake['Tired'])


# Tired vs. Sleep *the day before*

# In[7]:

# Get amount tired, label it as being one day earlier
# Therefore comparing 'Slept' from today with 'Tired' tomorrow 
dataf = rf.tokenDailyResampled(dayReporter, 'Mood', 'Tired', sampleType = 'mean')
dataf.index = dataf.index - pd.DateOffset(days = 1)

wake = wakeReporter.copy()
wake.index = wake['LocalTime']
wake.index = wake.index.date + pd.DateOffset(hours = 5)
wake.loc[:, 'SleepDuration'] = wake['SleepDuration'].dt.seconds/60.0/60.0

# Merge, and Remove Days Which Are Not Consecutive 
wake = wake.merge(dataf, how='inner', right_index=True, left_index=True)
wake = wake[getIdxTimeDifference(wake) == timedelta(days=1)]
sns.swarmplot(x=wake['Slept'], y=wake['Tired'])

sns.plt.figure()
sns.plt.scatter(x=wake['SleepDuration'], y=wake['Tired'])


# Linear regression: tired vs sleep time:

# In[ ]:

trainReporter, testReporter = sklearn.model_selection.train_test_split(dayReporter, test_size =0.2)
model1 = sklearn.linear_model.LinearRegression()

my_features = tmp['SleepDuration']
my_target = tmp['Tired']
model1.fit(tmp['SleepDuration'], tmp['Tired'])


# In[ ]:



